const  mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
passportLocalMongoose = require("passport-local-mongoose-email");

var UserSchema = new mongoose.Schema ({
    NP:{
      type: String,
         },
         
    Comp:{
      type: String,

    },

    Phone:{
      type: String,

    },

    Adr:{
      type: String,

    },

    email: {
      type: String,
      required: true,
      trim: true,
      unique: true
    },
    
     
    password: {type: String, required: true }
});
UserSchema.methods.encryptPassword = function(password){
  return bcrypt.hashSync(password, bcrypt.genSaltSync(5),null);
};

UserSchema.methods.validPassword = function(password){
  return bcrypt.compareSync(password,this.password);
};
UserSchema.plugin(passportLocalMongoose, { usernameField : 'email' });
module.exports = mongoose.model('User',UserSchema);