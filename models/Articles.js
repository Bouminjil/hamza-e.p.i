const  mongoose = require('mongoose');

var ArticleSchema = new mongoose.Schema ({
    imagePath: {
        type: String
    },
    title: {
        type: String, required: true
    },
    Description: {
        type: String, required:true
    },
    Price: {
        type: Number, required:true
    },
    categ: {
        type: String, required:true
        
    }
});

mongoose.model('Article',ArticleSchema);