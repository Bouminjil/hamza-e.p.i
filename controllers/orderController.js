// Require
require('../models/db');
require('../config/passport');
const express= require('express');
var router = express.Router()
var fs = require('fs-extra');
const mongoose= require('mongoose');
const Article = mongoose.model('Article');
const User = mongoose.model('User');
const Commande = mongoose.model('Commande');
const Slide = mongoose.model('Slide');
var Cart = require('../models/cart');
const puppeteer = require('puppeteer');
const PDFDocument = require('pdfkit')
const moment = require('moment');
const hbs = require('handlebars')
var pdf = require('html-pdf');
var html = fs.readFileSync('./views/Profile.hbs', 'utf8');
var options = { format: 'Letter' };
//var html2canvas = require('html2canvas');
var Canvas = require('canvas');





/*const compile = async function(templateName){
    const filePath =path.join(process.cwd(),'./views', `${templateName}.hbs`);
    const html = await fs.readFile(filePath,'utf-8');
    return hbs.compile(html);
};
hbs.registerHelper('dataFormat',function(value,format){
    return moment(value).format(format);
});*/



var passport = require('passport');
var csrf = require('csurf') ;
var csrfProtection = csrf({cookie:true}) ;


var mongoXlsx = require('mongo-xlsx');
var xlsx = require('node-xlsx');

const nodemailer = require('nodemailer');



 


path = require('path');
var multer = require('multer');

var Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "public/img/Articles");
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    }
});

var upload = multer({ storage: Storage }).array('imgUploader',12); //Field name and max count


var StorageEx = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "public/files");
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + path.extname(file.originalname) );
    }
});
var uploadEx = multer({ storage: StorageEx }).single('ExlUploader'); //Field name and max count
mongoose.set('useFindAndModify',false);


//USER/////////////////////
router.get('/EtCommande',isLoggedIn, async(req,res)=>{
    id = req.user.id
   // console.log(id);
    
  Commande.find().where("user", id).exec(function(err, commande) {
 
 let TAB =[]
 
 commande.forEach(element => {
      
   let  x = element.cart
  

    TAB.push(x)
     
    
  });
  console.log(TAB)

  
  
  
  res.render('EtatCommande',{CA:TAB});


    
  });

});


router.post('/Commande', isLoggedIn, function (req,res,next){
    
         
var cart=new Cart(req.session.cart);
//console.log(cart)
 var C = cart.generateArry();
/* CH = JSON.stringify(C)
 CH=CH.replace(/\[/g,'');
 CH=CH.replace(/\]/g,'');
 CH=CH.replace(/"/g, '');
 X= JSON.parse(CH);
 console.log(X)*/
  // var x= C.length  
      var commande = new Commande();
            
    commande.user = req.user;
    commande.cart = C;
    commande.totalPrice = cart.totalPrice
    commande.save()
     /* let X = [] 
     X.user = req.user ;
     X.CA = cart.items
     Tot=0
      for (let i = 0; i < x; i++) {
         Tot= Tot + C[i].Price
         
       } 
       console.log(cart.totalPrice)
       console.log(Tot)
     // console.log(X.CA)*/
        res.redirect('/EtCommande');
    
    
});

router.post('/UpdateUser', isLoggedIn,async(req,res)=>{
    idU = req.user.id
    
       
       
      /* upload(req, res, function(err,){*/
       
       let user =  User.findById(idU);
           
          /*  article.imagePath = req.files[0].originalname;*/
            user.NP = req.body.NP;
            user.Description = req.body.Comp;
            user.Phone = req.body.Phone;
            user.Adr = req.body.Adr;
            console.log(user)
           
        User.findOneAndUpdate({_id:idU},{$set:user},{new:true},function(err,doc){
            
            if (!err){
                res.redirect('/menu');
                
                
            } else {
                console.log('Update error '+err);
            }
        });
     

});



router.get('/logout',isLoggedIn, function(req, res,next) {
    
    req.logout();
    res.redirect('/');

  });




  router.get('/SignUp',csrfProtection, function (req, res ,next) {
    var messages = req.flash('error');
    console.log(messages)
  res.render('SignUp',{csrfToken: req.csrfToken(),messages:messages, hasErrors:messages});
});


router.post('/SignUp', passport.authenticate('local.signup',{
    successRedirect:'/profile',
    failureRedirect:'/SignUp',
    failureFlash : true
    
}));


router.get('/Signin',csrfProtection, function (req, res ,next) {
    var messages = req.flash('error');
    console.log(messages)
     res.render('SignIn',{csrfToken: req.csrfToken(),messages:messages, hasErrors:messages});
});


router.post('/SignIn', passport.authenticate('local.signin',{
    successRedirect:'/menu',
    failureRedirect:'/SignIn',
    failureFlash : true
  
}));




//Router
//get

router.post('/addPic',(req,res)=>{
    upload(req, res, function(err,){
        console.log(req.files[0].originalname);
        
    });
    res.redirect('/admin') 
})
router.post('/convert',(req,res)=>{
    
   
    uploadEx(req, res, function(err,){
    var FlN = req.file.filename;
    var xldata = __dirname + "/../public/files/"+FlN;
    console.log(xldata)
    var obj = xlsx.parse(xldata); 
    var x = Object.keys(obj).length;

    var article = new Article();
    var i ;
    for (let i = 0; i < x; i++) { 
         
        mongoXlsx.xlsx2MongoData(xldata,article, function(err, MongoData) {
             modifiedJson = JSON.stringify(MongoData[0]);
             final = JSON.parse(modifiedJson);
             
             var article = new Article();
             article.imagePath = final[i].imagePath;
             article.title =     final[i].title ;
             article.Description = final[i].Description;
             article.Price = final[i].Price;
             article.categ = final[i].categ
             article.save()            
            });
               
    } ;
    fs.unlink(xldata, (err) => {
        if (err) {
          console.error(err)
          return
        }
      
        //file removed
      })
    } );
         
    res.redirect('/admin') 
});


router.get ('/' , async(req,res)=>{
    res.render('Cover');
});

router.get('/menu',async(req,res) =>{
    let list = [];

    list.FrArt= await Article.find({categ:"Fraises"}) ;
    list.AoArt= await Article.find({categ:"EqMa"}) ;
    list.TrArt= await Article.find({categ:"Taraud"}) ;

    list.Sld = await Slide.find() ;

    res.render('menu', list);

});


    
router.get('/login', async (req, res) => {
    const users = await User.find({}).limit(10)
    res.render('login')
  });



 

router.get('/api/list',async(req,res) =>{
    let list = [];

    list.FrArt= await Article.find({categ:"Fraises"}) ;
    list.AoArt= await Article.find({categ:"EqMa"}) ;
    list.TrArt= await Article.find({categ:"Taraud"}) ;

    list.Sld = await Slide.find() ;

    res.json (list.FrArt)
});

router.get('/shopiing-cart',function(req,res ,next){
  if (!req.session.cart){
    user = req.user
      return res.render('Profile' , {user , product:null});
  }
 /* User.find({},(function(err, result) {
    if (err) throw err;
    console.log(result);
    
  }));*/

  sess = req.session;
  /*console.log(sess)*/
  user = req.user
  var cart=new Cart(req.session.cart);
  var gen = cart.generateArry();
  /*console.log(gen)*/
  res.render('Profile', {user,product: gen,totalPrice: cart.totalPrice});

});

router.get('/AddArt',(req,res)=>{
    res.render('AddArticle');
});

router.get('/AddSlider',(req,res)=>{
    res.render('AddSlider');
});

router.get('/Addimg',(req,res)=>{
    res.render('Addimg');
});





router.get('/admin',async(req,res)=>{

    let list = [];

    list.FrArt= await Article.find({categ:"Fraises"}) ;
    list.AoArt= await Article.find({categ:"EqMa"}) ;
    list.TrArt= await Article.find({categ:"Taraud"}) ;

    list.Sld = await Slide.find() ;
    res.render('admin',list);

   
});
router.get('/order/:id' ,(req,res)=>{
    Order.findById(req.params.id,(err,doc)=>{
        if (!err){
            res.render("orders",{order:doc});
        } else {
            console.log('Error findbyId: '+err);
        }
    });
});
router.get('/order/delete/:id',(req,res)=>{
    Cart.findByIdAndRemove(req.params.id,(err,doc)=>{
        if (!err) {
            res.redirect('/orderController');
        } else {
            console.log('Error in delete:'+ err);
        }
    });
});

router.get('/add-to-cart/:id', function(req,res,next){
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart :{});
    Article.findById(productId, function(err,product){
        if (err){
            return res.redirect('/menu');
        }
        cart.add(product, product.id);
        req.session.cart = cart;
        res.redirect('menu');
    });
});

router.get('/Del-One/:id', function(req,res,next){
    var productId = req.params.id;
    console.log(productId)
    var cart = new Cart(req.session.cart ? req.session.cart :{});
        cart.DelOne(productId);
        req.session.cart = cart;
        res.redirect('/shopiing-cart');
    
});

router.get('/Del-All/:id', function(req,res,next){
    var productId = req.params.id;
    console.log(productId)
    var cart = new Cart(req.session.cart ? req.session.cart :{});
        cart.removeItem(productId);
        req.session.cart = cart;
        res.redirect('/shopiing-cart');
    
});


router.get("/Delete/:id", function(req,res) {
    const idd=req.params.id
   Article.findByIdAndRemove({_id:idd},
       function (err,docs) {
       if (err) res.json(err);
       else res.redirect('/admin');
   });
   console.log(idd)
});

router.get('/Modif/:id',async(req,res)=>{
   
       const ident = req.params.id
       const article = await Article.findById(ident);
       res.render('AddArticle',article);
          
   });

   

   router.post('/Update/:_id',async(req,res)=>{
    idd = req.params._id
    console.log(idd)
       
       
       upload(req, res, function(err,){
       
        let article =  Article.findById(idd);
           
            article.imagePath = req.files[0].originalname;
            article.title = req.body.title;
            article.Description = req.body.Description;
            article.Price = req.body.Price;
            article.categ = req.body.categ;
            console.log(article)
           
        Article.findOneAndUpdate({_id:idd},{$set:article},{new:true},function(err,doc){
            
            if (!err){
                res.redirect('/menu');
                
                
            } else {
                console.log('Update error '+err);
            }
        });
     
});
});


 

//POST
/*
router.post('/login', function(req, res)  {
    session = req.session;
    if(session.uniqueID){
    }
    
    if(req.body.username== 'admin@admin' && req.body.password =='admin')
    {
        session.uniqueID = req.body.username;
    }
    res.redirect('redirects')
  })
  
 

  router.get('/redirects', function(req, res) {
    
    if(session.uniqueID)
    {
        res.redirect('admin');
    } else {
        res-encodeURI('who are you??');
    }
  });*/
  

 
router.post('/cart',(req,res)=>{
    insertOrder(req,res);
});
router.post('/order',(req,res)=>{
    updateOrder(req,res);
});

router.post('/AddArt',(req, res) => {
insertArt(req, res);
});


router.post("/Addslid",function(req,res){
    InsertSlid(req,res);
});

router.post('/contact', function (req, res) {
    let mailOpts, smtpTrans;
    smtpTrans = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: 'sahbi.berriri@gmail.com',
        pass: 'Sala7mela7**'
      }
    });
    mailOpts = {
      from: req.body.name + ' &lt;' + req.body.email + '&gt;',
      to: 'sahbi.berriri@gmail.com',
      subject: 'New message from contact form E.P.I',
      text: `${req.body.name} (${req.body.email}) says: ${req.body.message}`
    };
    smtpTrans.sendMail(mailOpts, function (error, response) {
      if (error) {
        res.render('contact-failure');
      }
      else {
        res.redirect('menu');
      }
    });
  });

 
  router.post('/PDF',async(req,res)=>{

    /*constconst  canvas = Canvas.createCanvas(500, 500);
    const ctx = canvas.getContext('2d');

ctx.fillRect(0,0,150,150);   // Draw a rectangle with default settings
ctx.save();                  // Save the default state

ctx.fillStyle = '#09F'       // Make changes to the settings
ctx.fillRect(15,15,120,120); // Draw a rectangle with new settings

ctx.save();                  // Save the current state
ctx.fillStyle = '#FFF'       // Make changes to the settings
ctx.globalAlpha = 0.5;    
ctx.fillRect(30,30,90,90);   // Draw a rectangle with new settings

ctx.restore();               // Restore previous state
ctx.fillRect(45,45,60,60);   // Draw a rectangle with restored settings

ctx.restore();               // Restore original state
ctx.fillRect(60,60,30,30);



     
   
     
    // Draw cat with lime helmet
  
    var out = fs.createWriteStream(__dirname + '/state.png')
  , stream = canvas.createPNGStream();

stream.on('data', function(chunk){
  out.write(chunk);
});
});
 
       genScreenshot();
  });

    html2canvas(document.body).then(function(canvas) {
        // Export the canvas to its data URI representation
        var base64image = canvas.toDataURL("image/png");
    
        // Open the image in a new window
        window.open(base64image , "_blank");
    });

    canvas.toBlob(function(blob) {
        // Generate file download
        window.saveAs(blob, "BonCommande.png");
    });
});
    
    user = req.user
  var cart=new Cart(req.session.cart);
  var gen = cart.generateArry();

    pdf.create(html,{user,product: gen,totalPrice: cart.totalPrice}).toFile('./BonCommande.pdf', function(err, res) {
        if (err) return console.log(err);
        console.log(res); // { filename: '/app/businesscard.pdf' }
      });
    });

   const doc = new PDFDocument()
    let filename = req.body.filename
    // Stripping special characters
    filename = encodeURIComponent(filename) + '.pdf'
    // Setting response to 'attachment' (download).
    // If you use 'inline' here it will automatically open the PDF
    res.setHeader('Content-disposition', 'attachment; filename="' + filename + '"')
    res.setHeader('Content-type', 'application/pdf')
    const content = req.body.content
    doc.y = 300
    doc.text(content, 50, 50)
    doc.pipe(res)
    doc.end()
    
    */


        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto("http://127.0.0.1:5000/shopiing-cart",{
            waitUntil :"networkidle2"
        });

       /* await page.pdf({
            path : 'BonCommande.pdf',
            format: 'A4',
            printBackground :true
        });*/

        await page.screenshot({path: 'buddy-screenshot.png'});
        await browser.close();
        console.log('done');
       
        
    });

    

/*function printPDF () {
    html2canvas(document.querySelector('#nodeToRenderAsPDF'))

    .then((canvas) => {
        let pdf = new jsPDF('p','mm','a4');
        pdf.addImage(canvas.toDataURL('image/png'),'PNG',0,0,211,298);
        pdf.save('Bon De Commande')
})
}*/

function genScreenshot() {
    html2canvas(document.body).then(function(canvas) {
        document.body.appendChild(canvas);
    });
}

function insertArt(req, res) {
    upload(req, res, function(err,) {
        var article = new Article();
        article.imagePath = req.files[0].originalname;
        article.title = req.body.title;
        article.Description = req.body.Description;
        article.Price = req.body.Price;
        article.categ = req.body.categ
        article.save((err, doc) => {
            if (!err)
               res.redirect('/');
            else {
                console.log('Err during record insertion : ' + err );
            }
        });       
        });
}



function InsertSlid (req,res){
    upload(req, res, function(err,) {
        name = req.file.filename;
        var slide = new Slide();
        slide.SlimagePath = req.files[0].originalname;
        slide.Sltitle = req.body.title;
        slide.SlDescription = req.body.Description;
        slide.save((err, doc) => {
            if (!err)
               res.redirect('/');
            else {
                console.log('Err during record insertion : ' + err );
            }
        });        console.log(name);


        });
}





//Function
/*function updateOrder(req,res){
    Order.findOneAndUpdate({ id:req.body._id},req.body,{new:true},(err,doc)=>{
        if (!err){
            res.redirect('/admin');
        } else {
            console.log('Update error '+err);
        }
    });
}


function insertOrder(req,res) {
    var d= new Date();
    var t=d.getTime();
    var counter= t;
    counter+=1;
    var order=new Order();
    order.total=req.body.total;
    order.order=counter;
    order.save((err,doc)=>{
        if (!err) {
            console.log('order: '+order);
            res.redirect('/admin');
        } else {
            console.log('Error insertOrder: '+err);
        }
    });
}*/

function isLoggedIn(req,res,next){
    if (req.isAuthenticated()){
        return next();
    }
    res.redirect('/menu')
}

function notLoggedIn(req,res,next){
    if (!req.isAuthenticated()){
        return next();
    }
    res.redirect('/')
}


module.exports= router;
