
var express= require('express');
var path= require('path');
var exphbs= require('express-handlebars');
const bodyparser= require('body-parser');
var cookieParser = require('cookie-parser');
var mongoose =require('mongoose');
mongoose.set('useCreateIndex', true);
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var validator = require('express-validator');


var orderController= require('./controllers/orderController');

var app=express();




mongoose.connect("mongodb://localhost:27017/EPIDb", {useNewUrlParser:true},(err)=>{
    if (!err){
        console.log('MongoDB connected');
    } else {
        console.log('error: '+err);
    }
});


require('./config/passport');


app.engine('hbs',exphbs({
    extname: 'hbs',
    defaultLayout: 'mainLayout',
    layoutsDir: __dirname+'/views/'
})) ;

app.set('view engine','hbs');


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: false
}));
//app.use(validator());
app.use(cookieParser())
app.use(session({
    secret:'Sala7mela7**',
    resave : false,
    saveUninitialized : false
}));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname,'/public')));
app.set('views',path.join(__dirname,'views'));

app.listen(5000,()=>{
    console.log('Server on port: 5000');
});

app.use(function(req,res,next){
    res.locals.login=req.isAuthenticated();
    next();
});
  
app.use('/',orderController);
app.use('/admin',orderController);